/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var data={};
var lon;
var lat;
var posFlag = true;

function getDate() {
    var now     = new Date(); 
    var year    = now.getFullYear();
    var month   = now.getMonth()+1; 
    var day     = now.getDate();
    var date = year+'/'+month+'/'+day;         
    return date;
}
function getTime() {
    var now     = new Date(); 
    var hour    = now.getHours();
    var min   = now.getMinutes();
    var sec     = now.getSeconds();
    var time = hour+':'+min+':'+sec;         
    return time;
}

// function dataSentSuccessfully(data) {
//     alert("Data Sent Success");
//     posFlag = false;
// }
// function requestApi(url, type, data, callback){
//     alert("Function is in api");    
//     $.ajax({
//         type: type,
//         url: url,
//         contentType: "application/json",
//         dataType: "json",
//         data: data,
        
//         beforeSend: function( xhr ) {

//         },
//         success: function(data, textStatus, jqXHR){
//             callback(data, textStatus, jqXHR);
//         },
//         complete: function() {

//         },
//         error: function(jqXHR, textStatus, errorThrown) {
//             alert("reqApiError: "+textStatus+":"+ errorThrown);
//         }
//     });
// }
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // navigator.geolocation.getCurrentPosition(onSuccess, onError);
        // setTimeout(getLocation, 3000);
                
        //var data={};
        alert("On device ready");
        setInterval(getLocation, 3000);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

function getLocation() {
    alert("Function is in geolocation");
    navigator.geolocation.getCurrentPosition(onSuccess, onError);            
}

function onSuccess(position) {
    /*if(posFlag == false) {
    var currentLat = Math.round(position.coords.latitude * 1000) / 1000;
    var currentLon = Math.round(position.coords.longitude * 1000) / 1000;
    $("#gpsData").html("lon = "+lon+" current lan = "+currentLon+" lat = "+lat+" currnet lat = "+currentLat);
    if(lon == currentLon && lat == currentLat) {
    alert("Position is the Same. No data is sent");
    } else {
    alert("Position Changed. Sending Data");
    posFlag = true;                    
    }
    }*/

    //if(posFlag == true) 

    lon = Math.round(position.coords.longitude * 1000) / 1000;
    lat = Math.round(position.coords.latitude * 1000) / 1000;                
    var myGPSDataString = 
    'Latitude: '          + position.coords.latitude          + '\n' +
    'Longitude: '         + position.coords.longitude         + '\n' +
    'Altitude: '          + position.coords.altitude          + '\n' +
    'Accuracy: '          + position.coords.accuracy          + '\n' +
    'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
    'Heading: '           + position.coords.heading           + '\n' +
    'Speed: '             + position.coords.speed             + '\n' +
    'Timestamp: '         + position.timestamp                + '\n';


    data = 
    {            
        "deviceid": "20130006",
        "latitude": position.coords.latitude,
        "longitude": position.coords.longitude,
        //"altitude": position.coords.altitude,
        "altitude": 0,
        //"accuracy": "'"+position.coords.accuracy+"'",            
        //"heading": position.coords.heading,
        "heading": 0,
        "speed": 0,
        //"speed": position.coords.speed,
        //"timestamp": position.timestamp
        "date": getDate(),
        "time": getTime()
    };                

    //$("#gpsData").html(myGPSDataString);
    alert(JSON.stringify(data));    // SHOWING GPS DATA
    //requestApi('http://192.168.100.123/vts/track/update', 'POST', JSON.stringify(data), dataSentSuccessfully);                
    // requestApi('http://kiteplexit.com/vts/track/update', 'POST', JSON.stringify(data), dataSentSuccessfully);                
}

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

       

// ip = 192.168.100.187/vts3/track/update


/*var data = {};
function onSuccess(){
 
    data = 
    {            
        "deviceid": "20130003",
        "latitude": "'"+position.coords.latitude+"'",
        "longitude": "'"+position.coords.longitude+"'",
        "altitude": "'"+position.coords.altitude+"'",
        "accuracy": "'"+position.coords.accuracy+"'",            
        "heading": "'"+position.coords.heading+"'",
        "speed": "'"+position.coords.speed+"'",
        "timestamp": "'"+position.timestamp+"'"
    };
}

function onError(error) {
    //alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
    alert("geolocation Error");
}

function requestApi(url, type, data, callback){    
    $.ajax({
        type: type,
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: data,
        
        beforeSend: function( xhr ) {
            
        },
        success: function(data, textStatus, jqXHR){
            callback(data, textStatus, jqXHR);
        },
        complete: function() {

        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("reqApiError: "+textStatus+":"+ errorThrown);
        }
    });
}

function dataSentSuccessfully(data) {
    //do nothing
    alert("success" + JSON.stringify(data));
}

$(document).ready(function() {
    alert("Nasty situation");

    data = 
    {            
        "deviceid": "20130003",
        "latitude": "100",
        "longitude": "200",
        "altitude": "300",
        "accuracy": "3000",            
        "heading": "123",
        "speed": "200",
        "timestamp": "200"
    };

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
    requestApi('http://192.168.100.186/vts3/track/update', 'POST', JSON.stringify(data), dataSentSuccessfully);
});