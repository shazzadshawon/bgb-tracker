var data = {};
var timeCycle = 30000;
var lon = 0;
var lat = 0;
var phoneNumber = 0;
var posFlag = true;
var netFlag = true;


function getDate() {
    var now     = new Date(); 
    var year    = now.getFullYear();
    var month   = now.getMonth()+1; 
    var day     = now.getDate();
    var date = year+'/'+month+'/'+day;         
    return date;
}

function getTime() {
    var now     = new Date(); 
    var hour    = now.getHours();
    var min   = now.getMinutes();
    var sec     = now.getSeconds();
    var time = hour+':'+min+':'+sec;         
    return time;
}

function requestApi(url, type, data, callback){
    $.ajax({
        type: type,
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: data,
        
        beforeSend: function( xhr ) {

        },
        success: function(data, textStatus, jqXHR){
            callback(data, textStatus, jqXHR);
        },
        complete: function() {

        },
        error: function(jqXHR, textStatus, errorThrown) {
            //alert("Request Api Error: "+textStatus+":"+ errorThrown);
            alert("Request Api Error Occured");
        }
    });
}

var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        clearDB();
        getPhoneNumberFromDB();       
    },
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};


function alertDismissed(){}


function initTracker()
{
    $('#deviceid').html('Device ID : '+phoneNumber).css('color', '#555');
    $('#gps_icon').css('color', '#009C00');
    setInterval(checkConnection, 3000);
    setInterval(getLocation, timeCycle);        
}

function getLocation() {
    if(netFlag == true){
        navigator.geolocation.getCurrentPosition(gpsSuccess, gpsError, {timeout:60000});     //DELAYING 60 SEC FOR GPS OFF
    }
}

function gpsSuccess(position) 
{  
    data = {            
        "deviceid": phoneNumber.substring(3),
        "latitude": position.coords.latitude,
        "longitude": position.coords.longitude,
        "altitude": 0,
        "heading": 0,
        "speed": 0,
        "date": getDate(),
        "time": getTime()
    };                

    //navigator.notification.alert(myGPSDataString, alertDismissed, 'GPS Data', 'Dismiss');   //SHOWING GPS DATA
    //alert(JSON.stringify(data));    // SHOWING GPS DATA
    
    dataSentSuccessfully(data);

    if(data.deviceid === ''){
        $('#netInfo').html('Wrong Phone Number');
        $('#netInfo').css('background', 'red');
    } else {
        requestApi('http://43.250.82.140/vts/track/update', 'POST', JSON.stringify(data), function(){
            posFlag = false;
            navigator.notification.beep(1);     //DEFAULT BEEP SOUND OF ANDROID
            $('#netInfo').html('Successfull send for deviceno: '+phoneNumber.substring(3));
            $('#netInfo').css('background-color','#FF7F00');
        });        // DOES NOT WORK UNDER SAME LAN ONLY MNET
    }
}
function dataSentSuccessfully(data){
    
    $('#show_lat').html('Latitude    :'+data.latitude).css('color', '#555');
    $('#show_lan').html('Longitude   :'+data.longitude).css('color', '#555'); 
    //alert(JSON.stringify(data));
    //navigator.notification.alert(JSON.stringify(data), alertDismissed, 'Server Response', 'Dismiss');
}
function gpsError(error) {
    //alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
    $('#netInfo').css('background', '#DB1516');
    $('#netInfo').html("Phone GPS is Off");
    $('#gps_icon').css('color', '#CC0000');
    /*if(error.code == PositionError.PERMISSION_DENIED){
        $('#netInfo').css('background', '#DB1516');
        $('#netInfo').html(error.message);
    }*/
}

function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    //alert('Connection type: ' + states[networkState]);
    $('#netInfo').html(states[networkState]);
    if(networkState == Connection.NONE){
        //disconnected
        $('#netInfo').html('Disconnected From Network');
        $('#netInfo').css('background', '#DB1516');
        $('#wifi_icon').css('color', '#CC0000');
        netFlag = false;
    } else if (networkState == Connection.WIFI){
        $('#netInfo').html('WIFI Connection. Please connect to 3G or 4G');
        $('#netInfo').css('background', '#FF7F00');
        $('#wifi_icon').css('color', '#009C00');
        netFlag = false;
    } else if(networkState == Connection.UNKNOWN || networkState == Connection.ETHERNET || networkState == Connection.CELL_3G){
        //connected
        $('#netInfo').html('Network Connected');
        $('#netInfo').css('background', '#00BB27');
        $('#wifi_icon').css('color', '#009C00');
        netFlag = true;
    }
}

function getPhoneNumberFromDB()
{
    var db = window.openDatabase("Database", "1.0", "GPS_TRACK", 200000);
    db.transaction(querydb, errorQuery);
    
}
function querydb(tx)
{
    tx.executeSql('SELECT * FROM PHONENUMBER',[],querySuccess,errorQuery);
}
function querySuccess(tx, results)
{
    var len = results.rows.length;
    phoneNumber = '' + results.rows.item(0).phonenumber;
    //alert('phonenumber from database: '+phoneNumber);
    if(phtmp == undefined || phtmp == null || !phtmp.trim().length)
    {
        findPhoneNumber();
    }else{
        initTracker();        
    }
}
function errorQuery(tx, err)
{
    //alert("Phonenumber: "+phoneNumber);
    //alert("Phonenumber len: "+phoneNumber);
    if(!phoneNumber){
        //alert('KO');
        findPhoneNumber();       
    }    
}
function clearDB(){
     var db = window.openDatabase("Database", "1.0", "GPS_TRACK", 200000);
    db.transaction(clearDBData, errorCB);
}
function clearDBData(tx)
{
    tx.executeSql('DROP TABLE IF EXISTS PHONENUMBER');
    //alert("Database Updated");
}
function createDB()
{
    var db = window.openDatabase("Database", "1.0", "GPS_TRACK", 200000);
    db.transaction(populateDB, errorCB);
}
function populateDB(tx)
{
    tx.executeSql('DROP TABLE IF EXISTS PHONENUMBER');
    tx.executeSql('CREATE TABLE IF NOT EXISTS PHONENUMBER (phonenumber)');
    tx.executeSql('INSERT INTO PHONENUMBER (phonenumber) VALUES ("'+phoneNumber+'")');
    //alert("Database Updated");
    initTracker();
}
function errorCB(tx, err)
{
    //alert("Your device not supported");
}

function findPhoneNumber(){
    window.plugins.sim.getSimInfo(simsuccessCallback, simerrorCallback);
}
function simsuccessCallback(result){
    var phtmp = result.phoneNumber;
    //alert("Sim phone Number"+phtmp);
    if(phtmp == undefined || phtmp == null || !phtmp.trim().length){
        getPhonenumberUserInput();
    } else {
        //alert("Sim phone number Saving");
        phoneNumber = '' + phtmp;
        createDB();
    }
}
function simerrorCallback(error){
    alert('Sim read Error');
    getPhonenumberUserInput();
}

function getPhonenumberUserInput()
{
    navigator.notification.prompt(
            'Please enter your Device Number',  // message
            onPrompt,                  // callback to invoke
            'Device Number',            // title
            ['Ok','Cancel'],             // buttonLabels
            ''                 // defaultText
    );
}
function onPrompt(results) {
    if(results.buttonIndex==1){
        alert("Your number is : " + results.input1);
        phoneNumber = '' + results.input1;        
        if((!isNaN(parseInt(phoneNumber))) && (phoneNumber.trim().length == 11))
        {            
            createDB();
        }else{
            alert("Device Number must be 11 digits.");
            navigator.notification.prompt(
                'Please enter your Device Number',  // message
                onPrompt,                  // callback to invoke
                'Device Number',            // title
                ['Ok','Cancel'],             // buttonLabels
                ''                 // defaultText
            );
        }
    }
}

$(document).ready(function(){
    $('#kill').click(function(){
        //alert("app will now be killed");
        navigator.notification.confirm("Sure to Kill?", confirmKill, "Kill", ['Kill', 'Back']);
        //navigator.app.exitApp();
    });

    function confirmKill(results){
        if(results == 1){
            navigator.app.exitApp();
        } else if(results == 2){
            return;
        }
    }
});
